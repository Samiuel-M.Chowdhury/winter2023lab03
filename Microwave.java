public class Microwave{
	public int maxTemperature;
	public String brand;
	public String colour;
	public void printBrandAndColor(String brand, String colour){
		System.out.println("The brand of your microwave is from "+brand +"and the color is "+ colour);
	}
	public void printTemperatureWarning(int temperature){
		System.out.println("Your microwave should not exceed "+(temperature-60)+"°C at minimum");
	}
}