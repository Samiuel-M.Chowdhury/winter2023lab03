import java.util.Scanner;
public class ApplienceStore{
	public static void main(String[] args){
		Scanner scan=new Scanner(System.in);
		Microwave micro=new Microwave();
		Microwave[] microArray= new Microwave[4];
		
		for(int i=0; i<microArray.length; i++){
			microArray[i] = new Microwave();
			System.out.println("What is the max temperature?");
			microArray[i].maxTemperature= scan.nextInt();
			System.out.println("What is the brand?");
			microArray[i].brand= scan.next();
			System.out.println("What colour is it?");
			microArray[i].colour= scan.next();
		}
		System.out.println(microArray[3].maxTemperature);
		System.out.println(microArray[3].brand);
		System.out.println(microArray[3].colour);
		
		micro.printBrandAndColor(microArray[0].brand,microArray[0].colour);
		micro.printTemperatureWarning(microArray[0].maxTemperature);
	}                            
}